SRC = $(wildcard *.md)

PDFS=$(SRC:.md=.pdf)
HTML=$(SRC:.md=.html)
TXT=$(SRC:.md=.txt)

all:    clean $(PDFS) $(HTML) $(TXT)

pdf:   clean $(PDFS)
html:  clean $(HTML)
txt:   clean $(TXT)

%.html: %.md
	python resume.py html $(GRAVATAR_OPTION) < $< | pandoc -t html -c $(PWD)/resume.css --self-contained -o $@

%.pdf:  %.md
	python resume.py html $(GRAVATAR_OPTION) < $< | pandoc -t html -c $(PWD)/resume.css --self-contained -o - | wkhtmltopdf --enable-local-file-access - $@

%.txt:  %.md
	python resume.py html $(GRAVATAR_OPTION) < $< | pandoc -t txt -c $(PWD)/resume.css --self-contained -o $@

ifeq ($(OS),Windows_NT)
  # on Windows
  RM = cmd //C del
else
  # on Unix
  RM = rm -f
endif

clean:
	$(RM) *.html *.pdf
