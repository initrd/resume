# Resume [![PDF](https://img.shields.io/badge/resume-pdf-green.svg)](https://gitlab.com/initrd/resume/-/raw/master/ashish-resume.pdf) [![TXT](https://img.shields.io/badge/resume-txt-blue.svg)](https://gitlab.com/initrd/resume/-/raw/master/ashish-resume.txt) [![HTML](https://img.shields.io/badge/resume-html-red.svg)](https://gitlab.com/initrd/resume/-/raw/master/ashish-resume.html)

This is a simple Markdown resumé template and pre-processing script that can be
used with [Pandoc](http://johnmacfarlane.net/pandoc/) to generate
professional-looking PDF, TXT, and HTML output.

The Markdown flavor supported is
[Pandoc markdown](http://johnmacfarlane.net/pandoc/README.html#pandocs-markdown).

## Dependencies

* Pandoc >= 1.9
* Python >= 2.7

## Usage

Clone the repo and create a new branch with a different Markdown file for your
resumé.

To generate PDF, TXT, and HTML versions of each .md file in the directory:

    $ make

In order to enable visually appealing display of contact information, the
Markdown is passed through a Python script that looks for contact details
beginning on the fourth line and moves them into a right-aligned, zero-height
box at the top of the document.  Lines with bullets (•) will be treated as
separate contact lines in the output.

By default, an image of your [Gravatar](http://www.gravatar.com) will be added
to the HTML and PDF resumé.  To avoid this:

    $ GRAVATAR_OPTION=--no-gravatar make

## Forked

From https://github.com/mwhite/resume
