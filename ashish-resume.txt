Bangalore
98804 43402
initrd@gmail.com

Ashish Vijayaram

Objective

-   To obtain a position that leverages my skills, experience, and
    accomplishments as a system administrator and operations engineer.

Skills

-   Proficient in Linux - primarily RHEL/CentOS, AWS, Docker,
    configuration management with Terraform/Puppet/Ansible, monitoring
    with LGTM/Nagios/InfluxDB, scripting with shell/Python, writing
    runbooks/documentation
-   Familiar with Kubernetes, Flux, ELK

Experience

-   Devops Lead | Spenmo | June 2021 - Present

    Devops Lead for Spenmo - a fintech company that handles payments and
    expenses for businesses. Managed three verticals - Devops/Infra,
    Devtools/Platform, IT.

    -   Devops/Infrastructure: Lead + IC
        -   Setup a serverless infrastructure in AWS using ECS Fargate
            using Terraform
            -   Design and implement blueprints for microservices using
                custom Terraform modules
            -   Implemented AWS Organization using Well Architected
                Framework using Terraform and Spacelift
        -   Migrated from multi-repo to monorepo of microservices +
            infrastructure
        -   Built out the monorepo and services using the 12-factor app
            methodology
        -   Scaled the company from four manual deploys a month to ~1000
            continuous deployments a month
        -   Gitops - Setup and maintained extensive CI/CD pipelines
            using Github Actions that covered:
            -   Pre-merge tests using a Docker Compose cluster and
                Sonarcloud
            -   Merge queue for a trunk-based monorepo
            -   Post-merge multi-arch build and deployment to Fargate,
                including DB migrations
            -   Self-hosted runners
            -   Continuous smoke and regression testing
        -   Implemented a multi-layer security model from development
            through operations
            -   sops + KMS for encrypting secrets in service
                configurations
            -   Minimal public exposure of infra/AWS resources
            -   Continuous monitoring of CIS benchmark-compliance alarms
                from Cloudtrail
            -   Endpoint security for all laptops
        -   Monitoring using RED/four golden signals methodolgies
        -   Oncall management for different engineering teams
        -   Documented all of the above :)
        -   Cost optimisation - reduced AWS spend by > 50% by
            identifying unused and redundant resources, fine tuning
            usage (RDS, Kinesis), etc.
        -   Led ISO 27001 and PCI compliance certifications for the
            company
    -   Devtools/Platform: Lead
        -   Great developer experiences
            -   Engineers go from a new laptop to commit in less than an
                hour
            -   Great local environment with LGTM, localstack, hot
                reloads for Go/web services, pre-commit hooks, etc.
            -   Ephemeral preview environments from Github PRs; Slack
                notifications with links to various services
            -   Slack integration for PRs, deployments, etc.
        -   Setup observability using the LGTM stack to obtain and push
            logs, traces, and metrics from Fargate to Grafana Cloud
    -   IT: Lead + IC
        -   Automated onboarding and offboarding employees using Jira
            (Service Management)
        -   Setup and managed centralised identity, device management,
            endpoint security
        -   Setup and managed a host of services for corp: Google
            Workspace, Slack, Atlassian, Zoom, etc.
        -   Automated and streamlined processes for ISO 27001 compliance

-   Sr. Systems Administrator | Mozilla, Mountain View | Jan 2016 - Feb
    2021

    Support full service lifecycle management, including regular
    updates, growth and refresh planning, and where necessary
    decommissions or migrations

    -   Manage Puppet infrastructure - setup Puppet servers,
        environments, migration from Puppet 3 to Puppet 6
    -   Migrate Puppet infrastructure to support CentOS 8
    -   Run and maintain Mozilla’s IRC network - irc.mozilla.org
    -   Built jumphosts for datacenters and offices, managed a local RPM
        package repository mirror for OS, local, and upstream packages
    -   Ansible playbooks for performing OS updates on servers, other
        adhoc tasks
    -   Setup and manage an internal instance of healthchecks.io for
        cron monitoring
    -   Managed and administered physical security software
        (C•CURE 9000) for offices

-   Site Reliability Engineer | Mozilla, Remote (Bangalore) | Apr 2011 -
    Jan 2016

    Maintenance of core server infrastructure, including network and
    physical layout, design and the integration required to scale
    Mozilla’s web services from tens to thousands of millions of
    clients. Effectively and quickly handled multiple incidents and
    outages.

    -   Participation in the operations center oncall system for
        production services in Mozilla’s datacenters and offices
    -   Respond to outages and issues affecting production services
    -   Handle hardware issues, software and OS upgrades
    -   Maintain effective operational documentation and runbooks
    -   Maintain the Nagios and related puppet modules, primary contact
        for monitoring
    -   Excellent troubleshooting and debugging skills

-   Technical Lead – Systems Administration | Yahoo!, Bangalore | Jan
    2010 - Apr 2011

    Part of Edge Operations - a global team that manages Yahoo’s CDN and
    data routing. The team primarily handled caching for Yahoo!
    properties with a global footprint. The services were customer
    facing and have high visibility both internally and externally.
    Primary responsibilities included:

    -   Managing a large fleet of Linux servers in a production network
        across world- wide datacenters
    -   Managing weekly configuration pushes that contain updated
        code/config to servers across the globe
    -   Primary contact for monitoring using Nagios and in-house
        solutions
    -   Maintain operational documentation and runbooks
    -   Work on config bugs (Bugzilla) and production support using
        Siebel ticketing
    -   Participate in 12x7 oncall shifts
    -   Work together with related teams - DNS, Networking and Systems -
        for production maintenance support

-   Operations Engineer | A9/Amazon.com, Bangalore | Jan 2008 – Dec 2009

    Part of Search Operations - a global 24x7 team responsible for
    administration and support of the A9 Search engine that powers
    Product Search on Amazon’s retail websites (US, CA, UK, DE, FR and
    JP). Primary responsibilities included:

    -   Upkeeping of large hardware and software architectures
    -   Routine large scale software deployments to the backend
        infrastructure
    -   Optimize production performance
    -   Configure and maintain monitoring services
    -   Participate in 12x7 on-call shifts as the primary engineer
        responsible for handling high severity events
    -   Develop automation tools and other duties

-   Systems Engineer | Amazon.com, Bangalore | Feb 2005 – Dec 2007

    Administering Linux and Windows workstations and servers

    -   Worked on setting up and managing services on both systems and
        network side, mostly on Linux
    -   Moved a mostly-Windows-based network to a complete Linux-based
        one
    -   Responsibilities included managing/supporting users and systems
        across both platforms and related activities
    -   Maintaining and scaling up LAN and WAN connectivity
    -   Gained a significant exposure to services hosted on Windows -
        DNS/DHCP, File and Print sharing, rudimentary Active Directory
        and Exchange (users, groups, lists, mailboxes, etc.)

Education

Bachelor of Engineering in Information Technology | Bangalore | Jul 2001
– May 2005

Built from initrd/resume@7415380b9316bf200702a8fde598945953c60f6a at Mon Apr 29 10:29:05 UTC 2024
